
global valor_verdadero cifras es contador x resultado iteracion ea anterior;

contador = 1; 

disp('=====================================================');
disp('Ejercicio 3: arctan(x)=x-(x^3)/3+(x^5)/5-(x^7)/7 ...'); 

format long;
valor_verdadero = atan((9/4)*pi);
disp('Valor verdadero');
disp(valor_verdadero);
cifras=input('¿Cuantas cifras significativas? ');

while(cifras < 0)
disp('Las cifras significativas no pueden ser negativas'); 
cifras = input('Cuantas cifras significativas?');
end

es=(0.5*10^(2-cifras));

%x = input('Ingrese el valor de "x" ');
 x=(9/4)*pi;
%disp('iteracion  Respuesta  Error Aprox.')

format long;
resultado =x;
iteracion =1;
ea = '-';

    fprintf('Iteracion          Resultado           Error Aprox \n');
    fprintf(' %.0f                 %g                 %g \n',iteracion,resultado,ea);
    format long;
    anterior = resultado;
    iteracion=iteracion+1;
    contador=contador+2;
   
    if(mod(iteracion,2)==0)
    resultado = resultado-((x^contador)/contador);
    else
       resultado = resultado+((x^contador)/contador); 
    end
    ea = (resultado-anterior)/resultado;
    fprintf(' %.0f            %.10f              %.10f \n',iteracion,resultado,ea);
    while(abs(ea)>es && iteracion<=21)
    anterior = resultado;
    iteracion=iteracion+1;
    contador=contador+2;
    if(mod(iteracion,2)==0)
    resultado = resultado-((x^contador)/contador);
    else
       resultado = resultado+((x^contador)/contador); 
    end
    ea = ((resultado-anterior)/resultado)*100;
     fprintf(' %.0f            %.10f              %.10f \n',iteracion,resultado,ea);
    end
    
 if(ea>es)
            disp('El valor real es: ');
            disp(valor_verdadero);
            disp('El nivel de tolerancia es: ');
            disp(es);
            disp('La serie converge demasiado lento.');
            disp('resultado: ');
            resultado = '---';
            disp(resultado);
            disp('error: ');
            ea = '---';
            disp(ea);
    else 
    disp('El valor real es: ');
    disp(valor_verdadero);
    disp('El nivel de tolerancia es: ');
    disp(es);
    disp('El valor calculado es: ');
    disp(resultado);
    disp('El error es: ');
    format short;
    disp(ea);
    end
    